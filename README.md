# Vyatta configuration/operational templates for Google Authenticator

This package adds Vyatta configuration and operation templates for the Google 
Authenticator command and PAM modules configuration. Google Authenticator must be 
installed to use these commands and configurations.

Current Debian packages can be downloaded from the my [ftp space](ftp://ftp.drivehq.com/mdailous/PublicFolder/). Hopefully, one day it will be available via the Debian FTP server mirrors.

## History


This project came out of the fact that when upgrading UBNT routers, user-installed
software and user-specific settings weren't persisted. Per UBNT, the proper place to
store persisted user settings is in the /config/user-data/ folder. By default, Google
Authenticator looks for the secret file (.google_authenticator) in the home directory
of the user that is authenticating. The Google Authenticator framework also allow the use
of the "secret" option to specify a non-default location where the secret file should be
used. This requires the modification of the PAM module definition in the /etc/pam.d/sshd
file, as well as adding the option to the "google-authenticator" command. To mitigate
user error, this project handles all the dirty work for the user.

# Dependencies

The following projects are are required to be installed for this package to operate
correctly.

* [Google Authenticator PAM module](https://github.com/google/google-authenticator-libpam)

# Further Reading

* This [link](https://help.ubnt.com/hc/en-us/articles/204976234-EdgeMAX-How-can-I-add-new-packages-and-features-to-EdgeOS-) was used setup the project as a debian package. The debian packaging is a derivative of
work provided by [Daniil Baturin](https://github.com/vyos/vyatta-dummy)
* Information about the VyOS CLI is available [here](http://wiki.vyos.net/wiki/CLI_internals), [here](https://help.ubnt.com/hc/en-us/articles/204976224-EdgeMAX-Add-commands-to-EdgeOS), [here](http://wiki.vyos.net/wiki/Configuration_mode_templates) and [here](http://wiki.vyos.net/wiki/Cli-shell-api).
* A plethora of Vyos examples can also be found at the [Vyos Github Repo](https://github.com/vyos).
